import Vue from "vue";
import Vuex from "vuex";
import router from "../router";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    category: ""
  },
  mutations: {
    SET_CATEGORY(state, category) {
      state.category = category;
      router.push("/categories/" + category);
    }
  },
  actions: {},
  modules: {}
});
